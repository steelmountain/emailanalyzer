# Email Dissector

This app is meant for the unfortunate ones that need to analyse emails that might contain something fishy or harmful. My experience is that even though the big tech might be able to analyse emails to decide whether to quarantine something or not they generally don't provide a very good view to their analysis. On the other hand they seem to get it wrong fairly often.

Anyway, if you, like me, receive a constant stream of suspicious emails reported by your (human) colleagues running them through this app might be helpful to make a quick first-pass analysis of the email.

## Online version

Just go to https://kurre.gitlab.io/emaildissector for the full app. It doesn't store anything and the only server calls it makes are to Cloudflare DoH API to check DKIM, SPF and DMARC (Coming soon), and to Google's safe browsing API to check whether any URLs found in the emails point to suspicious domains (Coming soon).

## Offline version

You can build it yourself with the instructions below. No server needed, just run it from `public` or copy to wherever you want.

## Todo

-   ~~Attachments handling~~
-   Simple analysis based on recipient address etc
-   DKIM, SPF and DMARC verification
-   URL safety check
-   Fetch & Ajax hijack for protection and analysis

## Development

### Installing dependencies

```bash
$ npm install # or pnpm install or yarn install
```

### Dev server

```bash
$ npm dev # or npm start
```

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>

### Building for production

```bash
$ npm run build
```

Builds the app for production to the `public` folder.
