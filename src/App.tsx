import {Component, createSignal, For, onMount} from 'solid-js'

import AttachmentsTable from './components/AttachmentsTable'
import FileSelector from './components/FileSelector'
import HeaderTable from './components/HeaderTable'
import Preview from './components/Preview'
import RelativeDate from './components/RelativeDate'

import {EmailData, parseEmail} from './EmailParser'

const App: Component = () => {
    const [data, setData] = createSignal('')
    const [emailData, setEmailData] = createSignal<EmailData>({
        headers: {},
        urls: [],
        emails: [],
        attachments: [],
        parts: [],
    })

    const [defang, setDefang] = createSignal(true)
    const [activeTab, setActiveTab] = createSignal('preview')

    onMount(() => {
        const paramString = window.location.search
        const queryString = new URLSearchParams(paramString)
        if (queryString.has('msg')) {
            const rawData = queryString.get('msg')
            if (!rawData) {
                return
            }
            const data = atob(decodeURIComponent(rawData))
            setData(data)
            const email = parseEmail(data)
            setEmailData(email)
        }
    })

    const tabs = [
        {id: 'analysis', title: 'Analysis'},
        {id: 'preview', title: 'Preview'},
        {id: 'headers', title: 'Headers'},
        {id: 'attachments', title: 'Attachments'},
        {id: 'parts', title: 'Parts'},
        {id: 'source', title: 'Raw Source'},
    ]

    const headerFields = [
        {
            label: 'Subject',
            key: 'subject',
            render: (value: string) => value,
        },
        {
            label: 'From',
            key: 'from',
            render: (value: string) => value,
        },
        {
            label: 'To',
            key: 'to',
            render: (value: string) => value,
        },
        {
            label: 'Reply-To',
            key: 'reply-to',
            render: (value: string) => value,
        },
        {
            label: 'Return-Path',
            key: 'return-path',
            render: (value: string) => value,
        },
        {
            label: 'Date',
            key: 'date',
            render: (value: string) => <RelativeDate dateStr={value} />,
        },
    ]

    return (
        <div class="flex h-screen overflow-hidden">
            <FileSelector setData={setData} setEmailData={setEmailData} />

            <div class="flex-1 overflow-hidden flex flex-col">
                <div class="p-4 flex-shrink-0">
                    <div class="grid grid-cols-2 gap-x-4 bg-white shadow-sm rounded-md p-2">
                        <For each={headerFields}>
                            {(field) => (
                                <div class="flex items-baseline gap-2">
                                    <div class="text-xs font-medium text-gray-500 uppercase tracking-wider shrink-0 w-24">
                                        {field.label}
                                    </div>
                                    <div class="text-sm text-gray-900 truncate">
                                        {field.render(
                                            emailData().headers[field.key] ||
                                                'Not specified',
                                        )}
                                    </div>
                                </div>
                            )}
                        </For>
                    </div>

                    <div class="mt-4">
                        <div class="flex items-center justify-between border-b border-gray-200 pb-4">
                            <nav class="flex space-x-4" aria-label="Tabs">
                                <For each={tabs}>
                                    {(tab) => (
                                        <button
                                            onClick={() => setActiveTab(tab.id)}
                                            class={`py-1 px-2 border-b-2 text-sm font-medium ${
                                                activeTab() === tab.id
                                                    ? 'border-blue-500 text-blue-600'
                                                    : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
                                            }`}
                                        >
                                            {tab.title}
                                        </button>
                                    )}
                                </For>
                            </nav>
                            <label class="inline-flex items-center cursor-pointer">
                                <input
                                    type="checkbox"
                                    class="sr-only peer"
                                    checked={defang()}
                                    onChange={(e) => {
                                        setDefang(!defang())
                                    }}
                                />
                                <div class="relative w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 rounded-full peer peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-blue-600"></div>
                                <span class="ms-3 text-sm font-medium text-gray-900">
                                    Defang URLs
                                </span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="flex-1 overflow-auto p-4">
                    {activeTab() === 'analysis' && (
                        <div class="space-y-4">
                            <div>
                                <h2 class="text-xl font-bold mb-2">URLs</h2>
                                <div class="space-y-1">
                                    <For each={emailData().urls}>
                                        {(url, _index) => (
                                            <p class="text-gray-700">{url}</p>
                                        )}
                                    </For>
                                </div>
                            </div>
                            <div>
                                <h2 class="text-xl font-bold mb-2">
                                    Email addresses
                                </h2>
                                <div class="space-y-1">
                                    <For each={emailData().emails}>
                                        {(email, _index) => (
                                            <p class="text-gray-700">{email}</p>
                                        )}
                                    </For>
                                </div>
                            </div>
                        </div>
                    )}
                    {activeTab() === 'preview' && (
                        <Preview defang={defang()} email={emailData()} />
                    )}
                    {activeTab() === 'headers' && (
                        <HeaderTable email={emailData()} />
                    )}
                    {activeTab() === 'attachments' && (
                        <AttachmentsTable parts={emailData().attachments} />
                    )}
                    {activeTab() === 'parts' && (
                        <AttachmentsTable parts={emailData().parts} />
                    )}
                    {activeTab() === 'source' && (
                        <pre class="text-left whitespace-pre-wrap bg-gray-50 p-3 rounded-lg">
                            {data()}
                        </pre>
                    )}
                </div>
            </div>
        </div>
    )
}

export default App
