// import { EMAIL_REGEX, URL_REGEX } from '../utils'

import {Buffer} from 'buffer'

export const EMAIL_REGEX =
    /(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/g
export const NAME_REGEX = /^(.*) ?</

export const URL_REGEX =
    /(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#/%=~_|$?!:,.]*\)|[A-Z0-9+&@#/%=~_|$])/gim

type EmailHeaders = Record<string, string>

export interface EmailPart {
    mime: string
    data?: string
    text?: string
    name: string
    location?: string
}

export interface EmailData {
    headers: EmailHeaders
    htmlPart?: string
    textPart?: string

    urls: string[]
    emails: string[]

    parts: EmailPart[]

    attachments: EmailPart[]

    source?: string
}

function splitAtFirstOccurrence(
    separator: string,
    toSplit: string,
): [string, string] {
    const idx = toSplit.indexOf(separator)

    if (idx === -1) {
        return [toSplit, '']
    }

    return [
        toSplit.substring(0, idx),
        toSplit.substring(idx + separator.length),
    ]
}

function decodeQuotedPrintable(encoded: string) {
    return (
        encoded
            // https://tools.ietf.org/html/rfc2045#section-6.7, rule 3:
            // “Therefore, when decoding a `Quoted-Printable` body, any trailing white
            // space on a line must be deleted, as it will necessarily have been added
            // by intermediate transport agents.”
            .replace(/[\t\x20]$/gm, '')
            // Remove hard line breaks preceded by `=`. Proper `Quoted-Printable`-
            // encoded data only contains CRLF line  endings, but for compatibility
            // reasons we support separate CR and LF too.
            .replace(/=(?:\r\n?|\n|$)/g, '')
            // Decode escape sequences of the form `=XX` where `XX` is any
            // combination of two hexidecimal digits. For optimal compatibility,
            // lowercase hexadecimal digits are supported as well. See
            // https://tools.ietf.org/html/rfc2045#section-6.7, note 1.
            .replace(/=([a-fA-F0-9]{2})/g, (_, $1) => {
                const codePoint = parseInt($1, 16)
                return String.fromCharCode(codePoint)
            })
    )
}

function parseContentType(contentType: string) {
    // logInfo('Parsing content type', contentType)
    if (!contentType) {
        return {
            mime: 'text/plain',
        }
    }
    const [mime, rest] = splitAtFirstOccurrence(';', contentType)
    return {
        mime: mime.toLowerCase(),
        boundary: rest.match(/boundary="?([^";]*)"?/i)?.[1],
        charset: rest.match(/charset="?([^";]*)[";]?/i)?.[1],
        name: rest.match(/name="?([^";]*)"?/i)?.[1],
    }
}

function parseContentDisposition(contentDisposition: string) {
    // logInfo('Parsing disposition', contentDisposition)
    const [disposition, rest] = splitAtFirstOccurrence(';', contentDisposition)
    return {
        disposition,
        name: rest.match(/filename="?([^"]*)"?/)?.[1],
    }
}

function stringToDataArray(data: string): Int8Array {
    const byteArr = new Int8Array(data.length)
    for (let i = 0; i < byteArr.length; i++) {
        byteArr[i] = data.charCodeAt(i)
    }
    return byteArr
}

function convertCharset(data: string, charset: string) {
    const byteArr = stringToDataArray(data)
    // logInfo('Converting', data, 'to', charset)
    const output = new TextDecoder(charset).decode(byteArr)
    return output
}

export function decodeEncodedWord(data: string) {
    const ret = data.replace(
        /=\?(\S+)\?(\S+)\?(\S+)\?=/g,
        (
            match: string,
            charset: string,
            encoding: string,
            word: string,
        ): string => {
            // logInfo(match, charset, encoding, word)

            if (!charset || !encoding || !word) {
                return match || ''
            }

            if (encoding.toLowerCase() === 'b') {
                word = atob(word)
            } else if (encoding.toLowerCase() === 'q') {
                word = decodeQuotedPrintable(word.replace(/_/g, ' '))
            }

            word = convertCharset(word, charset)
            return word
        },
    )
    return ret
}

function parseHeaders(email: string) {
    const headers: EmailHeaders = {}
    // logInfo('parsing headers', email)
    let currentHeaderName = ''
    for (const row of email.split(/\r\n|\n/)) {
        // logInfo(row)
        // End of headers
        if (row.trim() === '') {
            return headers
        }

        // Wrapped value
        if (row.startsWith('\t') || row.startsWith(' ')) {
            headers[currentHeaderName] +=
                '\r\n\t' + decodeEncodedWord(row.trim())
            continue
        }

        const [name, value] = splitAtFirstOccurrence(':', row)

        currentHeaderName = name.toLowerCase()

        if (headers[currentHeaderName] !== undefined) {
            headers[currentHeaderName] +=
                '\r\n----\r\n' + decodeEncodedWord(value.trimStart())
            continue
        }

        headers[currentHeaderName] = decodeEncodedWord(value.trimStart())
        // logInfo(currentHeaderName, value)
    }

    return headers
}

function parseBody(email: string, headers: EmailHeaders): Partial<EmailData> {
    //logInfo('Headers', headers)
    const contentType = parseContentType(headers['content-type'])
    const contentDisposition =
        (headers['content-disposition'] &&
            parseContentDisposition(headers['content-disposition'])) ||
        null

    const encoding = headers['content-transfer-encoding']?.toLowerCase()

    // logInfo('contenttype', contentDisposition, contentType, encoding)

    // Apparently sometimes there's only \n
    const properParts = splitAtFirstOccurrence('\r\n\r\n', email)
    const parts =
        properParts[1].length > 1
            ? properParts
            : splitAtFirstOccurrence('\n\n', email)

    // logInfo('parts', parts, contentDisposition, contentType, encoding)

    if (encoding === 'quoted-printable') {
        parts[1] = decodeQuotedPrintable(parts[1])
    }

    if (encoding === 'base64') {
        parts[1] = Buffer.from(parts[1], 'base64').toString('binary')
        //logInfo('base64', parts[1])
    }

    if (
        contentType.charset &&
        (contentType.charset.toLowerCase() !== 'utf-8' ||
            !['7bit', '8bit'].includes(encoding?.toLowerCase()))
    ) {
        parts[1] = convertCharset(parts[1], contentType.charset)
    }

    const contentLocation =
        headers['content-location'] ||
        headers['content-id']?.replace('<', '').replace(/>$/, '')

    const currentPart = {
        name: contentDisposition?.name || 'unknown',
        mime: contentType.mime,
        data: parts[1],
        text: contentType.charset || encoding === '8bit' ? parts[1] : undefined,
        location:
            contentDisposition?.disposition === 'inline'
                ? contentLocation
                : undefined,
    }

    if (contentType.mime.startsWith('multipart/')) {
        const invalidItems = [
            'This is a multi-part message in MIME format',
            '--',
            'This message is in MIME format. Since your mail reader does not understand this format, some or all of this message may not be legible.',
        ]
        const alternatives = parts[1]
            .split('--' + contentType.boundary)
            .map((v) => v.trim())
            .filter((v) => v.length > 0 && !invalidItems.includes(v))

        // logInfo('Alts', contentType.mime, contentType.boundary, alternatives)

        let ret: EmailData = {
            headers,
            attachments: [],
            parts: [],
            urls: [],
            emails: [],
        }

        for (const alt of alternatives) {
            // logInfo('alt', alt)
            // Unsure why, but apparently sometimes some of the content gets duplicated
            if (alt.startsWith('--')) {
                if (alt.trim() !== '--') {
                    console.error(
                        'Found extra data after end of multipart list!',
                    )
                }
                break
            }
            if (alt.startsWith('This is a multi-part message in MIME format')) {
                continue
            }
            const altEmail = parseEmail(alt)
            // logInfo(ret, altEmail)
            ret = {
                ...altEmail,
                ...ret,
                attachments: ret.attachments.concat(altEmail.attachments),
                parts: ret.parts.concat(altEmail.parts),
                urls: [...new Set(ret.urls?.concat(altEmail.urls || []))],
                emails: [...new Set(ret.emails?.concat(altEmail.emails || []))],
            }
            // logInfo('alts ret', ret)
        }
        return ret
    }

    if (
        contentDisposition &&
        ['attachment', 'inlineXX'].includes(contentDisposition.disposition)
    ) {
        // logInfo('returning an attachment')
        return {
            attachments: [currentPart],
        }
    }

    const urls = [...parts[1].matchAll(URL_REGEX)].map((url) => url[0])

    const emails = [...parts[1].matchAll(EMAIL_REGEX)].map((url) => url[0])

    // logInfo('urls', urls, emails)

    if (contentType.mime === 'text/plain') {
        return {textPart: parts[1], urls, emails, parts: [currentPart]}
    }
    if (contentType.mime === 'text/html') {
        return {htmlPart: parts[1], urls, emails, parts: [currentPart]}
    }

    return {parts: [currentPart]}
}

export function parseEmail(email: string): EmailData {
    const headers = parseHeaders(email)
    const body = parseBody(email, headers)
    body.emails?.sort()
    body.urls?.sort()
    return {
        headers,
        urls: [],
        emails: [],
        ...body,
        attachments: body.attachments || [],
        parts: body.parts || [],
        source: email,
    }
}
