export function defangURL(url: string): string {
    return url
        .replace(/^http(s?):\/\/(.*)$/, 'hxxp$1[://]$2')
        .replace(/\./g, '[.]')
}

const noReplacement = ['http://www.w3.org/TR/html4/strict.dtd', '4.01']

export function defangAllURLs(text: string): string {
    const urlMatches = text.matchAll(
        /(?:(?:https?|ftp):\/\/)?[\w/\-?=%.]+\.[\w]+(?::\d*)?(?:\/[^\/][\w/\-&?=%.]*)?/g,
    )

    const urls = [...urlMatches]

    for (const urlMatch of urls) {
        const url = urlMatch[0]
        if (noReplacement.includes(url)) {
            continue
        }
        const defanged = defangURL(url)

        text = text.replace(url, defanged)
    }

    return text
}
