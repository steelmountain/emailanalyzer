import {For} from 'solid-js'
import {EmailPart} from '../EmailParser'

interface AttachmentTableProps {
    parts: EmailPart[]
}

function generateDataURL(data: string, mime: string) {
    return `data:${mime};base64,${btoa(data)}`
}

function AttachmentPreview(props: {attachment: EmailPart}) {
    if (props.attachment.mime.startsWith('image/') && props.attachment.data) {
        return (
            <img
                class="max-w-xs"
                src={generateDataURL(
                    props.attachment.data,
                    props.attachment.mime,
                )}
            />
        )
    }
    return (
        <pre class="whitespace-pre-wrap text-sm">
            {props.attachment.text ||
                'Data is binary with mime type ' + props.attachment.mime}
        </pre>
    )
}

export default function AttachmentsTable(props: AttachmentTableProps) {
    return (
        <div class="overflow-x-auto">
            <table class="w-full divide-y divide-gray-200">
                <thead class="bg-gray-50">
                    <tr>
                        <th class="px-3 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            File name
                        </th>
                        <th class="px-3 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Mime type
                        </th>
                        <th class="px-3 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Data length
                        </th>
                        <th class="px-3 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Preview
                        </th>
                    </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                    <For each={props.parts}>
                        {(attachment, _i) => (
                            <tr class="hover:bg-gray-50">
                                <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-900">
                                    {attachment.name}
                                </td>
                                <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-500">
                                    {attachment.mime}
                                </td>
                                <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-500">
                                    {attachment.data?.length}
                                </td>
                                <td class="px-3 py-2 text-sm text-gray-500">
                                    <AttachmentPreview
                                        attachment={attachment}
                                    />
                                </td>
                            </tr>
                        )}
                    </For>
                </tbody>
            </table>
        </div>
    )
}
