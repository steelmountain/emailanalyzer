import {Setter, createSignal, For} from 'solid-js'
import {EmailData, parseEmail} from '../EmailParser'

interface FileSelectorProps {
    setData: Setter<string>
    setEmailData: Setter<EmailData>
}

interface FileItem {
    name: string
    content: string
    email: EmailData
}

export default function FileSelector(props: FileSelectorProps) {
    const [selectedFiles, setSelectedFiles] = createSignal<FileItem[]>([])
    const [currentFile, setCurrentFile] = createSignal<string | null>(null)
    const [isDragging, setIsDragging] = createSignal(false)

    const handleFileChange = (files: FileList | null) => {
        if (!files) {
            return
        }

        let firstFile: FileItem | null = null
        let processedFiles = 0
        const totalFiles = files.length

        Array.from(files).forEach((file) => {
            const reader = new FileReader()
            reader.onload = function (e) {
                const contents = (e?.target?.result || 'ERROR').toString()
                const email = parseEmail(contents)
                const fileItem = {
                    name: file.name,
                    content: contents,
                    email: email,
                }

                if (!firstFile) {
                    firstFile = fileItem
                }

                setSelectedFiles((prev) => [...prev, fileItem])
                processedFiles++

                // After all files are processed, select the first one
                if (processedFiles === totalFiles && firstFile) {
                    selectFile(firstFile)
                }
            }
            reader.readAsText(file)
        })
    }

    function handleFileDrop(file: File) {
        if (file?.type === 'message/rfc822') {
            file.text().then((data) => {
                const email = parseEmail(data)
                const fileItem = {
                    name: file.name,
                    content: data,
                    email: email,
                }

                setSelectedFiles((prev) => [...prev, fileItem])

                // If this is the first file, select it
                if (selectedFiles().length === 0) {
                    selectFile(fileItem)
                }
            })
        } else {
            console.log('Unknown file', file?.type, file?.name)
        }
    }

    function dropHandler(ev: DragEvent) {
        if (!ev || !ev.dataTransfer) {
            return
        }
        ev.preventDefault()
        setIsDragging(false)

        if (ev.dataTransfer.items) {
            ;[...ev.dataTransfer.items].forEach((item) => {
                if (item.kind === 'file') {
                    const file = item.getAsFile()
                    if (file) {
                        handleFileDrop(file)
                    }
                }
            })
        }
    }

    function selectFile(file: FileItem, focusButton = true) {
        props.setData(file.content)
        props.setEmailData(file.email)
        setCurrentFile(file.name)

        if (focusButton) {
            // Find and focus the button after a short delay to ensure DOM is updated
            setTimeout(() => {
                const button = document.querySelector(
                    `button[data-filename="${file.name}"]`,
                ) as HTMLButtonElement
                button?.focus()
            }, 0)
        }
    }

    function handleKeyDown(event: KeyboardEvent) {
        const files = selectedFiles()
        if (files.length === 0) return

        const currentIndex = files.findIndex((f) => f.name === currentFile())
        if (currentIndex === -1) return

        if (event.key === 'ArrowDown') {
            event.preventDefault()
            const nextIndex = Math.min(currentIndex + 1, files.length - 1)
            selectFile(files[nextIndex])
        } else if (event.key === 'ArrowUp') {
            event.preventDefault()
            const prevIndex = Math.max(currentIndex - 1, 0)
            selectFile(files[prevIndex])
        }
    }

    return (
        <div class="w-64 min-w-64 flex-shrink-0 h-screen bg-gray-50 border-r border-gray-200 flex flex-col">
            <div class="p-4 flex-shrink-0">
                <div
                    class={`relative w-full border-2 border-dashed rounded-lg p-4 text-center transition-colors cursor-pointer ${
                        isDragging()
                            ? 'border-blue-500 bg-blue-50'
                            : 'border-gray-300 hover:border-blue-500'
                    }`}
                    onDrop={dropHandler}
                    onDragOver={(ev) => {
                        ev.preventDefault()
                        setIsDragging(true)
                    }}
                    onDragLeave={() => setIsDragging(false)}
                >
                    <input
                        type="file"
                        accept="message/rfc822"
                        multiple
                        class="absolute inset-0 w-full h-full opacity-0 cursor-pointer"
                        onChange={(e) => {
                            if (e instanceof Event && 'files' in e.target) {
                                handleFileChange(e.target.files)
                            }
                        }}
                    />
                    <div class="space-y-2">
                        <div class="text-sm text-gray-600">
                            Drop email files here, or click to select
                        </div>
                    </div>
                </div>
            </div>

            {selectedFiles().length > 0 && (
                <>
                    <div class="px-4 flex-shrink-0">
                        <div class="border-t border-gray-200"></div>
                    </div>
                    <div class="flex-1 overflow-y-auto px-4 py-2">
                        <div class="space-y-2">
                            <For each={selectedFiles()}>
                                {(file) => (
                                    <button
                                        data-filename={file.name}
                                        onClick={() => selectFile(file)}
                                        onKeyDown={handleKeyDown}
                                        class={`w-full text-left p-2 text-sm border rounded-md shadow-sm transition-colors ${
                                            currentFile() === file.name
                                                ? 'bg-blue-50 border-blue-500 text-blue-700 hover:bg-blue-100'
                                                : 'bg-white border-gray-200 text-gray-900 hover:bg-gray-50'
                                        } focus:outline-none focus:ring-2 focus:ring-blue-500 break-all`}
                                    >
                                        {file.name}
                                    </button>
                                )}
                            </For>
                        </div>
                    </div>
                </>
            )}
        </div>
    )
}
