import {For} from 'solid-js'
import {EmailData} from '../EmailParser'

interface HeaderTableProps {
    email: EmailData
}

export default function HeaderTable(props: HeaderTableProps) {
    return (
        <div class="overflow-x-auto">
            <table class="w-full divide-y divide-gray-200">
                <thead class="bg-gray-50">
                    <tr>
                        <th class="px-3 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Header Name
                        </th>
                        <th class="px-3 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Header Value
                        </th>
                    </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                    <For each={Object.keys(props.email.headers).sort()}>
                        {(hdr, _i) => (
                            <tr class="hover:bg-gray-50">
                                <td class="px-3 py-2 whitespace-nowrap text-sm text-gray-900 max-w-[200px] truncate">
                                    {hdr}
                                </td>
                                <td class="px-3 py-2 text-sm text-gray-500">
                                    <pre class="whitespace-pre-wrap">
                                        {props.email.headers[hdr]}
                                    </pre>
                                </td>
                            </tr>
                        )}
                    </For>
                </tbody>
            </table>
        </div>
    )
}
