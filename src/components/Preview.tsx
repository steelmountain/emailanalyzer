import {createSignal, For} from 'solid-js'
import {EmailData} from '../EmailParser'
import {defangAllURLs} from '../UrlDefanger'

interface PreviewProps {
    email: EmailData
    defang: boolean
}

export default function Preview(props: PreviewProps) {
    const [activeSubTab, setActiveSubTab] = createSignal('side-by-side')

    const subTabs = [
        {id: 'side-by-side', title: 'Side by Side'},
        {id: 'html', title: 'HTML'},
        {id: 'plain-text', title: 'Plain Text'},
    ]

    const NoContentMessage = ({type}: {type: string}) => (
        <div class="bg-white rounded-md shadow-sm p-3">
            <div class="flex items-center justify-center h-[800px] text-gray-500">
                <p>No {type} content available in this email</p>
            </div>
        </div>
    )

    const HtmlPreview = () => (
        <div class="bg-white rounded-md shadow-sm p-3">
            {props.email.htmlPart ? (
                <iframe
                    class="w-full h-[800px] border-0"
                    srcdoc={
                        props.defang
                            ? defangAllURLs(props.email.htmlPart)
                            : props.email.htmlPart
                    }
                />
            ) : (
                <NoContentMessage type="HTML" />
            )}
        </div>
    )

    const PlainTextPreview = () => (
        <div class="bg-white rounded-md shadow-sm p-3">
            {props.email.textPart ? (
                <pre class="w-full h-[800px] whitespace-pre-wrap overflow-auto text-sm">
                    {props.defang
                        ? defangAllURLs(props.email.textPart)
                        : props.email.textPart}
                </pre>
            ) : (
                <NoContentMessage type="plain text" />
            )}
        </div>
    )

    return (
        <div class="space-y-2">
            <div class="border-b border-gray-200">
                <nav class="-mb-px flex space-x-4" aria-label="Preview Tabs">
                    <For each={subTabs}>
                        {(tab) => (
                            <button
                                onClick={() => setActiveSubTab(tab.id)}
                                class={`py-1 px-2 border-b-2 text-sm font-medium ${
                                    activeSubTab() === tab.id
                                        ? 'border-blue-500 text-blue-600'
                                        : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
                                }`}
                            >
                                {tab.title}
                            </button>
                        )}
                    </For>
                </nav>
            </div>

            <div>
                {activeSubTab() === 'html' && <HtmlPreview />}
                {activeSubTab() === 'plain-text' && <PlainTextPreview />}
                {activeSubTab() === 'side-by-side' && (
                    <div class="grid grid-cols-2 gap-4">
                        <HtmlPreview />
                        <PlainTextPreview />
                    </div>
                )}
            </div>
        </div>
    )
}
