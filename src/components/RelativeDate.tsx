import {createSignal} from 'solid-js'

interface RelativeDateProps {
    dateStr: string
}

function getRelativeTimeString(date: Date): string {
    const now = new Date()
    const diffInSeconds = Math.floor((now.getTime() - date.getTime()) / 1000)
    const diffInMinutes = Math.floor(diffInSeconds / 60)
    const diffInHours = Math.floor(diffInMinutes / 60)
    const diffInDays = Math.floor(diffInHours / 24)
    const diffInMonths = Math.floor(diffInDays / 30)
    const diffInYears = Math.floor(diffInDays / 365)

    if (diffInSeconds < 60) {
        return 'just now'
    } else if (diffInMinutes < 60) {
        return `${diffInMinutes} minute${diffInMinutes === 1 ? '' : 's'} ago`
    } else if (diffInHours < 24) {
        return `${diffInHours} hour${diffInHours === 1 ? '' : 's'} ago`
    } else if (diffInDays < 7) {
        return `${diffInDays} day${diffInDays === 1 ? '' : 's'} ago`
    } else if (diffInDays < 30) {
        const weeks = Math.floor(diffInDays / 7)
        return `${weeks} week${weeks === 1 ? '' : 's'} ago`
    } else if (diffInMonths < 12) {
        return `${diffInMonths} month${diffInMonths === 1 ? '' : 's'} ago`
    } else {
        return `${diffInYears} year${diffInYears === 1 ? '' : 's'} ago`
    }
}

function formatDateTime(date: Date): string {
    return date.toLocaleString('en-US', {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        hour12: false,
        timeZoneName: 'short',
    })
}

export default function RelativeDate(props: RelativeDateProps) {
    const date = new Date(props.dateStr)
    const relativeTime = getRelativeTimeString(date)
    const exactTime = formatDateTime(date)
    const [showExact, setShowExact] = createSignal(false)
    return (
        <div
            class="relative inline-block"
            onMouseEnter={() => setShowExact(true)}
            onMouseLeave={() => setShowExact(false)}
        >
            {showExact() ? exactTime : relativeTime}
        </div>
    )
}
